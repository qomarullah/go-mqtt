package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"os"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	rotatelogs "github.com/lestrrat/go-file-rotatelogs"
)

func connect(clientId string, uri *url.URL) mqtt.Client {
	opts := createClientOptions(clientId, uri)
	client := mqtt.NewClient(opts)
	token := client.Connect()
	for !token.WaitTimeout(3 * time.Second) {
	}
	if err := token.Error(); err != nil {
		log.Fatal(err)
	}
	return client
}

func createClientOptions(clientId string, uri *url.URL) *mqtt.ClientOptions {
	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("tcp://%s", uri.Host))
	opts.SetUsername(uri.User.Username())
	password, _ := uri.User.Password()
	opts.SetPassword(password)
	opts.SetClientID(clientId)
	return opts
}

func listen(uri *url.URL, topic string) {
	client := connect("sub", uri)
	client.Subscribe(topic, 0, func(client mqtt.Client, msg mqtt.Message) {
		//fmt.Printf("* [%s] %s\n", msg.Topic(), string(msg.Payload()))
		event := &Event{Timestamp: time.Now().Format(time.RFC3339), Topic: msg.Topic(), Message: string(msg.Payload())}
		eventLog, _ := json.Marshal(event)
		log.Println(string(eventLog))

	})
}

type Event struct {
	Timestamp string `json:"timestamp"`
	Topic     string `json:"topic"`
	Message   string `json:"message"`
}

func main() {
	logPath := os.Getenv("LOG")
	if logPath == "" {
		logPath = "logs"
	}
	createLogPath(logPath)

	uri, err := url.Parse(os.Getenv("URL"))
	if err != nil {
		log.Fatal(err)
	}
	topic := uri.Path[1:len(uri.Path)]
	if topic == "" {
		topic = "#"
	}
	rl, _ := rotatelogs.New(logPath + "/log.%Y%m%d%H")
	log.SetOutput(rl)
	log.SetFlags(0)

	go listen(uri, topic)
	client := connect("pub", uri)
	timer := time.NewTicker(1 * time.Second)
	for t := range timer.C {
		client.Publish(topic, 0, false, t.String())
	}

}
func createLogPath(logPath string) {
	_, err := os.Stat(logPath)
	if os.IsNotExist(err) {
		errDir := os.MkdirAll(logPath, 0755)
		if errDir != nil {
			log.Fatal(err)
		}

	}
}
